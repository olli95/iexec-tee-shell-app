# Starting from a base image supported by SCONE  
FROM node:14-alpine3.11

# install your dependencies
RUN mkdir /app

COPY ./src /app

ENTRYPOINT [ "/bin/bash", "entrypoint.sh"]